# Daniel's ReadMe (:construction: Under construction :construction:)
Hi :wave: I'm Daniel and this README tells you a little bit about me and my peculiarities. My hope is that by sharing this, you will understand me be a bit better. Thanks for reading!! :pray:

Please feel free to contribute to this page by opening a [merge request](https://gitlab.com/dparker/readme/-/merge_requests/new).

## What I'm currently working
The current week's issue is visible [here](https://gitlab.com/dparker/dparker/-/issues). Past week's issues are closed.

## A little about me
- I am an Aussie 🇦🇺 with Spanish 🇪🇸 heritage living in Ecuador 🇪🇨
- I speak
    - English ⭐️ ⭐️ ⭐️ ⭐️ ⭐️ 
    - Spanish ⭐️ ⭐️ ⭐️
- I have lived in Ecuador since Jan 2018, firstly in [Quito](https://www.google.com/maps/place/Quito) (2850m / 9350ft above :ocean: level), and now in a small town called [Atuntaqui](https://www.google.com/maps/place/Atuntaqui) (2,413m / 7916ft above :ocean: level).

### Sports
- I follow Formula 1 🏎 very closely, watching every qualifying and race.
    - Favourite drivers:
        - Daniel Ricciardo 🇦🇺
        - Max Verstappen 🇳🇱 🇧🇪 for the Driver's Championship 🏆
        - Lando Norris 🇬🇧
        - Carlos Sainz 🇪🇸
        - Fernando Alonso 🇪🇸
    - Favourite team
        - McLaren
- Cycling :bike:
    - I used to cycle a lot with my dad, but haven't been doing it since I moved to Ecuador. I Miss it a lot.
- Tennis 🎾
    - I played competitive tennis when I was a teen and still like to watch it on TV, especially the Australian Open.

### Australia
- I was born and grew up in Geelong, Victoria.
- I moved to Melbourne in 2012 when I started university.
- Melbourne is my favorite city, mostly because of its great cafe's, restaurants and people!!
- In 2014-15 I played trumpet in a Colombian Cumbia band lead by my Spanish teacher, performing in bars and halls around the northern suburbs of Melbourne.

### Ecuador
- Ecuador is on the equator and we don't have traditional seasons like countries closer to the poles
- The temperature where I live doesn't vary much throughout the year
    - ![Quito Temperature in Celcius](https://gitlab.com/dparker/dparker/-/raw/main/images/quito-temp-c.png)

### My spare time
- PC Gaming (casual)
    - Battlefield
    - Age of Empires IV
    - Northgard
    - Flight simulator (Microsoft or XPlane)
- Reading
- TV
- Programming

## My working style

![Simpli5 Energy Map](https://gitlab.com/dparker/dparker/-/raw/main/images/simpli5-energy-map.png)

I'm going to split this up into two sections, programming and managing

### Programming
As a programmer, I'm very exploratory and high execute. My preference is to create prototypes to better understand unknowns and feasability than to just talk about it. I am very comfortable with unknowns and with refactoring. I learn best from documentation and doing, but I also like watching short courses and reading books or blogs occasionally to learn.

I'm a polyglot programmer and am not scared of moving between technologies and using new technologies that I'm not familiar with. My strong belief is that the same patterns appear everywhere and once you've encountered them once, you can transfer that knowledge to learning new technologies.

#### Cons of being me
- I'm quick to propose solutions, and it take effort for me to keep ideas high level until the solutioning starts. Call me out on this.
- I can sometimes trivialise things that are easy for me to visualise and that's not intentional. Please ask me to elaborate if I'm doing this.
- I am not risk averse and probably have a much higher tolerance for risk than others, however I have found from experience that the quicker you can jump into implementation, the quicker you'll identify unknowns and risks. Analysis paralysis is **real**, people!!

### Managing (Aspiring)
- I love helping people learn and improve themselves
- I am not a micromanager, but if I don't see movement, I do tend to jump in and get involved. If you feel that I am stepping on your toes, that's not intentional, I can just revert to engineer mode inadvertently.
- I am an aspiring manager, so I'm not going to do everything perfectly the first time. Help me learn by giving me concrete feedback.
- I have bursts of Examine and Excite (see diagram above) to do those types of activities, but I'm in flow when performing Explore and Execute activities.

## At work, nothing makes me happier than
- Nerding out on technical discussions
- Architecting solutions
- Building helpful tools
- Helping others thrive

## Mutual Agreement Aspiration

## Strengths

## Weaknesses and mitigation

## My career goals

## My usual day
- Wake up at 7am
- Monday, Wednesday, Friday mornings I do an hour of exercise
- Make breakfast and coffee
- At 8am I'm usually going through emails and Slack and planning my day
- I have 1:1s on Tuesday and Thursday mornings with my team between 8:30am and 10am
- My mornings and middle of my day are usually pretty full of meetings and meeting planning
- I squeeze in lunch between meetings
- My afternoons I try to fit in tasks that I had to do, starting with things for my team, followed by project work and team planning and docs
- I finish up work at around 4pm
- Relax, and do something with my wife, play with the dogs, and PC gaming.

# Attribution
This README format and weekly status format were inspired by [Michelle Torres](https://gitlab.com/michelletorres/readme)
